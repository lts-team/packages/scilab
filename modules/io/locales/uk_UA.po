# Ukrainian translation for scilab
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the scilab package.
#
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
# Yuri Chornoivan <yurchor@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2013-12-06 19:41+0000\n"
"Last-Translator: Yuri Chornoivan <yurchor@gmail.com>\n"
"Language-Team: Ukrainian <translation@linux.org.ua>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 17413)\n"
"Language: uk\n"

#, c-format
msgid "%s: Can not read input argument #%d.\n"
msgstr "%s: не вдалося прочитати вхідний параметр №%d.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: A string expected.\n"
msgstr ""
"%s: помилковий розмір вхідного параметра №%d: слід використовувати рядок.\n"

#, c-format
msgid "%s: Memory allocation error.\n"
msgstr "%s: помилка виділення пам’яті.\n"

#, c-format
msgid "%s: The file \"%s\" does not exist.\n"
msgstr "%s: файла з назвою %s не існує.\n"

#, c-format
msgid "%s: The file does not exist.\n"
msgstr "%s: такого файла не існує.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A scalar expected.\n"
msgstr ""
"%s: помилковий тип вхідного параметра №%d: слід використовувати скалярний "
"тип.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: An integer expected.\n"
msgstr ""
"%s: помилкове значення вхідного параметра №%d: слід використовувати ціле "
"значення.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A string expected.\n"
msgstr ""
"%s: помилковий тип вхідного параметра №%d: слід використовувати тип "
"«string».\n"

#, c-format
msgid "%s: Undefined environment variable %s.\n"
msgstr "%s: невизначена змінна середовища %s.\n"

#, c-format
msgid "%s: No more memory.\n"
msgstr "%s: доступну пам’ять вичерпано.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: String expected.\n"
msgstr ""
"%s: помилковий тип вхідного параметра №%d: слід використовувати тип "
"«string».\n"

#, c-format
msgid "%s: Feature %s is obsolete.\n"
msgstr "%s: можливість %s є застарілою.\n"

msgid "Warning"
msgstr "Попередження"

#, c-format
msgid "%s: See help('load') for the rationale.\n"
msgstr "%s: пояснення наведено на сторінці help('load').\n"

#, c-format
msgid ""
"%s: This feature will be permanently removed in Scilab %s\n"
"\n"
msgstr ""
"%s: цю можливість буде остаточно вилучено з Scilab %s\n"
"\n"

#, c-format
msgid "%s: Scilab 6 will not support the file format used.\n"
msgstr ""
"%s: у Scilab 6 підтримки використаного формату файлів передбачено не буде.\n"

#, c-format
msgid ""
"%s: Please quote the variable declaration. Example, save('myData.sod',a) "
"becomes save('myData.sod','a').\n"
msgstr ""
"%s: будь ласка, вказуйте змінну під час оголошення у лапках. Приклад: "
"save('myData.sod',a) має бути записано як save('myData.sod','a').\n"

#, c-format
msgid "%s: See help('save') for the rational.\n"
msgstr ""
"%s: докладніше про причину можна дізнатися за допомогою команди "
"help('save').\n"

#, c-format
msgid "Undefined environment variable %s.\n"
msgstr "Невизначена змінна середовища %s.\n"

msgid "Too many files opened!\n"
msgstr "Відкрито забагато файлів!\n"

#, c-format
msgid "File \"%s\" already exists.\n"
msgstr "Файл з назвою «%s» вже існує.\n"

#, c-format
msgid "\"%s\" directory write access denied.\n"
msgstr "Запис до каталогу «%s» заборонено.\n"

#, c-format
msgid "File \"%s\" does not exist.\n"
msgstr "Не виявлено файла з назвою «%s».\n"

#, c-format
msgid "File \"%s\" read access denied.\n"
msgstr "Читання файла «%s» заборонено.\n"

msgid "unmanaged error by v2cunit.\n"
msgstr "непридатна до обробки помилка v2cunit.\n"

#, c-format
msgid "%s: handle no more valid ignored.\n"
msgstr "%s: елемент керування, що вже не діє, проігноровано.\n"

#, c-format
msgid "%s : Invalid entity %s\n"
msgstr "%s: некоректний елемент %s\n"

#, c-format
msgid "%s: Legend does not fit with the current context. Skipped\n"
msgstr ""
"%s: умовні позначення не відповідають поточному контексту. Пропущено.\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d expected.\n"
msgstr ""
"%s: помилкова кількість вхідних параметрів: слід використовувати %d.\n"

#, c-format
msgid "%s: variable '%s' does not exist in '%s'.\n"
msgstr "%s: змінної «%s» немає у «%s».\n"

#, c-format
msgid "%s: Wrong number of input argument.\n"
msgstr "%s: помилкова кількість вхідних параметрів.\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d to %d expected.\n"
msgstr ""
"%s: помилкова кількість вхідних параметрів: слід використовувати від %d до "
"%d.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: '%s' value expected.\n"
msgstr ""
"%s: помилкове значення вхідного параметра №%d: слід використовувати значення "
"«%s».\n"

#, c-format
msgid "%s: Wrong number of input argument(s): %d expected.\n"
msgstr "%s: помилкова кількість вхідних параметрів: мало бути %d.\n"

#, c-format
msgid "%s: Wrong number of output argument(s).\n"
msgstr "%s: помилкова кількість вихідних параметрів.\n"

#, c-format
msgid "%s: The system interpreter does not answer..."
msgstr "%s: системний інтерпретатор команд не відповідає…"

#, c-format
msgid "%s: error during %s execution"
msgstr "%s: помилка під час спроби виконання %s"

#, c-format
msgid ""
"%s: The command failed with the error code %s and the following message:\n"
msgstr ""
"%s: спроба виконати команду завершилася помилкою з кодом %s та таким "
"повідомленням:\n"
